import os
import argparse
import pickle
from pathlib import Path

import clip
from evaluation.helpers.vggface import *
from torchvision.models import resnet50

from evaluation.helpers.kNN import *
from evaluation.helpers.metrics import *

def simple_evaluator(model, 
                     device,
                     dataloader,
                     checkpoint,
                     experiment_path,
                     sample_idx=0,
                     **args,
                     ):
  '''
  Takes a trained diffusion model from 'checkpoint' and evaluates its performance on the test 
  dataset 'dataloader' w.r.t. the three most important perfromance metrics; FID, IS, KID. We continue
  the progress of our evaluation function for the LDM upscalaer and may update this function accordingly.
    
  checkpoint:      Name of the saved pth. file containing the trained weights and biases  
  experiment_path: Path to the experiment folder where the evaluation results will be stored  
  dataloader:      Loads the test dataset for evaluation
  sample_idx:      Integer that denotes which sample directory sample_{sample_idx} from the checkpoint model shall be used for evaluation
  '''

  checkpoint_path = f'{experiment_path}trained_ddpm/{checkpoint}'
  # create evaluation directory for the complete experiment (if first time sampling images)
  output_dir = f'{experiment_path}evaluations/'
  os.makedirs(output_dir, exist_ok=True)

  # create evaluation directory for the current version of the trained model
  model_name = os.path.basename(checkpoint_path)
  epoch = re.findall(r'\d+', model_name)
  if epoch:
      e = int(epoch[0])
  else:
      raise ValueError(f"No digit found in the filename: {filename}")
  model_dir = os.path.join(output_dir,f'epoch_{e}')
  os.makedirs(model_dir, exist_ok=True)

  # create the evaluation directory for this evaluation run for the current version of the model 
  eval_dir_list = [d for d in os.listdir(model_dir) if os.path.isdir(os.path.join(model_dir, d))]
  indx_list = [int(d.split('_')[1]) for d in eval_dir_list if d.startswith('evaluation_')]
  j = max(indx_list, default=-1) + 1
  eval_dir = os.path.join(model_dir, f'evaluation_{j}')
  os.makedirs(eval_dir, exist_ok=True)
  
  # Compute Metrics   
  eval_path = os.path.join(eval_dir, 'eval.txt')
 
  # get sampled images
  transform = transforms.Compose([transforms.ToTensor(), transforms.Lambda(lambda x: (x * 255).type(torch.uint8))])
  sample_path =  os.path.join(f'{experiment_path}samples/',f'epoch_{e}',f'sample_{sample_idx}')
  ignore_tensor = f'image_tensor{j}'
  images = []
  for samplename in os.listdir(sample_path):
        if samplename == ignore_tensor:
            continue
        img = Image.open(os.path.join(sample_path, samplename))
        img = transform(img)
        images.append(img)
  # split them into batches for GPU memory
  generated = torch.stack(images).to(device)
  generated_batches = torch.split(generated, dataloader.batch_size)
  nr_generated_batches = len(generated_batches)
  nr_real_batches = len(dataloader)

  # Init FID, IS and KID scores
  fid = FrechetInceptionDistance(normalize = False).to(device)
  iscore = InceptionScore(normalize=False).to(device)
  kid = KernelInceptionDistance(normalize=False, subset_size=32).to(device)

  # Update scores for the full testing dataset w.r.t. the sampled batches
  for idx,(data, _) in enumerate(dataloader):
    data = data.to(device)
    fid.update(data, real=True)
    kid.update(data, real=True)
    if idx < nr_generated_batches:
        gen = generated_batches[idx].to(device)
        fid.update(gen, real=False)
        kid.update(gen, real=False)
        iscore.update(gen)

  # If there are sampled images left, add them too
  for idx in range(nr_real_batches, nr_generated_batches):
    gen = generated_batches[idx].to(device)
    fid.update(gen, real=False)
    kid.update(gen, real=False)
    iscore.update(gen)
 
  # compute total FID, IS and KID 
  fid_score = fid.compute()
  i_score = iscore.compute()
  kid_score = kid.compute()

   # store results in txt file
  with open(str(eval_path), 'a') as txt:
    result = f'FID_epoch_{e}_sample_{sample_idx}:'
    txt.write(result + str(fid_score.item()) + '\n')
    result = f'KID_epoch_{e}_sample_{sample_idx}:'
    txt.write(result + str(kid_score) + '\n')
    result =  f'IS_epoch_{e}_sample_{sample_idx}:'
    txt.write(result + str(i_score) + '\n')



def ddpm_evaluator(experiment_path, realpath, genpath, data='lhq', size=128, arch='clip', mode='kNN', k=3, sample=10, name_appendix='', fid='no'):
    
    #device = "mps" if torch.backends.mps.is_available() else "cpu"
    device = "cuda" if torch.cuda.is_available() else "cpu"
    print('device:', device)

    path_to_real_images = realpath
    path_to_generated_images = genpath
    dataset = data
    arch = arch
    mode = mode
    k_kNN = k
    sample = sample
    name_appendix = name_appendix
    fid_bool = fid
    size = size
    
    print('Start')
    output_path = Path(os.path.join(experiment_path,'eval_output'))
    if not output_path.is_dir():
        os.mkdir(output_path)

    # output path
    os.chdir(output_path)
    txt_filename = 'evaluation_' + dataset + '_' + arch + '_' + mode + '-' + name_appendix + '.txt'
    with open(txt_filename, 'w') as f:
        f.write(f'Path to real images: {path_to_real_images}\n')
        f.write(f'Path to generated images: {path_to_generated_images}\n')
        f.write(f'Experiment on {dataset} dataset with images of resolution {size}x{size}\n')
        f.write(f'Using {arch} model to extract features\n')
        f.write(f'Plot of {mode} on {sample} samples\n')
        f.write(f'Quantitative metrics computed: {fid_bool}\n')
        
    
    # load data
    path_to_training_images = os.path.join(path_to_real_images, 'train')
    path_to_test_images = os.path.join(path_to_real_images, 'test')

    if fid_bool == 'yes':

        # metrics eval
        eval_images = image_to_tensor(path_to_test_images, device=device)
        generated = image_to_tensor(path_to_generated_images, device=device)
    
        print('Computing FID, KID & inception scores...')
        fid_score, kid_score, is_score = compute_scores(eval_images, generated, device)
        with open(txt_filename, 'a') as f:
            f.write(f'FID score: {fid_score}\n')
            f.write(f'KID score: {kid_score}\n')
            f.write(f'Inception score: {is_score}\n')

        print('Computing Clean FID scores...')
        clean_fid_score, clip_clean_fid_score = clean_fid(path_to_test_images, path_to_generated_images)
        with open(txt_filename, 'a') as f:
            f.write(f'Clean FID score: {clean_fid_score}\n')
            f.write(f'Clean FID score with CLIP features: {clip_clean_fid_score}\n')
        
        print('Computing FID infinity and IS infinity scores...')
        fid_infinity, is_infinity = fid_inf_is_inf(path_to_test_images, path_to_generated_images) 
        with open(txt_filename, 'a') as f:
            f.write(f'FID Infinity score: {fid_infinity}\n')
            f.write(f'IS Infinity score: {is_infinity}\n')

        #train_images = image_to_tensor(path_to_training_images, device=device)

        #print('Computing PSNR and SSIM scores...')
        #psnr_score, ssim_score = compute_ssim_psnr_scores(train_images, generated, device)
        #with open(txt_filename, 'a') as f:
        #    f.write(f'PSNR score: {psnr_score}\n')
        #    f.write(f'SSIM score: {ssim_score}\n')
        
    
    print('Loading model...', arch)   
    feature_flag = False
    
    # kNN-based eval
    if dataset == 'lhq':
        print('Dataset ', dataset)
        pth = '/home/wn455752/repo/evaluation/features/lhq'
        # load pretrained ResNet50 
        if arch == 'cnn':
            print('Loading pretrained ResNet50...')
            path_to_pretrained_weights = '/home/wn455752/repo/evaluation/pretrained/resnet50_places365_pretrained/resnet50_places365_weights.pth'
            weights = torch.load(path_to_pretrained_weights)
            model = resnet50().to(device)
            model.load_state_dict(weights)
            transform = transforms.Compose([transforms.ToTensor(),                       # transform PIL.Image to torch.Tensor
                                            transforms.Lambda(lambda x: x * 255)])       # scale values to VGG input range
            with torch.no_grad():
                model.eval()
            print('Checking for existing training dataset features...')
            # check for saved dataset features
            name_pth = Path(os.path.join(pth, 'resnet_features/real_name_list'))
            if name_pth.is_file():
                with open(name_pth, 'rb') as fp:
                    real_names = pickle.load(fp)
            feature_pth = Path(os.path.join(pth, 'resnet_features/real_image_features.pt'))
            if name_pth.is_file():
                print('Loading existing training dataset features...')
                real_features = torch.load(feature_pth, map_location="cpu")
                real_features = real_features.to(device)
                feature_flag = True
        # load CLIP
        elif arch == 'clip':
            print('Loading pretrained CLIP...')
            model, transform = clip.load("ViT-B/32", device=device)
            # check for saved dataset features
            print('Checking for existing training dataset features...')
            name_pth = Path(os.path.join(pth, 'clip_features/real_name_list'))
            if name_pth.is_file():
                with open(name_pth, 'rb') as fp:
                    real_names = pickle.load(fp)
            feature_pth = Path(os.path.join(pth, 'clip_features/real_image_features.pt'))
            if name_pth.is_file():
                print('Loading existing training dataset features...')
                real_features = torch.load(feature_pth, map_location="cpu")
                real_features = real_features.to(device)
                feature_flag = True



    elif dataset == 'faces':
        print('Dataset ', dataset)
        presaved_feature_pth = '/home/wn455752/repo/evaluation/features/faces' 
        # load pretrained VGGFace
        if arch == 'cnn':
            print('Loading pretrained VGGFace...')
            path_to_pretrained_weights = '/home/wn455752/repo/evaluation/pretrained/vggface_pretrained/VGG_FACE.t7'
            model = VGG_16().to(device)
            model.load_weights(path=path_to_pretrained_weights)
            transform = transforms.Compose([transforms.ToTensor(),                   # transform PIL.Image to torch.Tensor
                                        transforms.Resize((224,224)),                # resize to VGG input shape
                                        transforms.Lambda(lambda x: x * 255)])       # scale values to VGG input range
            with torch.no_grad():
                model.eval()
            
            # check for saved dataset features
            print('Checking for existing training dataset features...')
            name_pth = Path(os.path.join(presaved_feature_pth, 'vggface_features/real_name_list'))
            if name_pth.is_file():
                with open(name_pth, 'rb') as fp:
                    real_names = pickle.load(fp)
            feature_pth = Path(os.path.join(presaved_feature_pth, 'vggface_features/real_image_features.pt'))
            if name_pth.is_file():
                print('Loading existing training dataset features...')
                real_features = torch.load(feature_pth, map_location="cpu")
                real_features = real_features.to(device)
                feature_flag = True

        # load CLIP
        elif arch == 'clip':
            print('Loading pretrained CLIP...')
            model, transform = clip.load("ViT-B/32", device=device)
            # check for saved dataset features
            print('Checking for existing training dataset features...')
            name_pth = Path(os.path.join(presaved_feature_pth, 'clip_features/real_name_list'))
            if name_pth.is_file():
                with open(name_pth, 'rb') as fp:
                    real_names = pickle.load(fp)
            feature_pth = Path(os.path.join(presaved_feature_pth, 'clip_features/real_image_features.pt'))
            if name_pth.is_file():
                print('Loading existing training dataset features...')
                real_features = torch.load(feature_pth, map_location="cpu")
                real_features = real_features.to(device)
                feature_flag = True

    knn = kNN()
    # get images
    if not feature_flag:
        print('Collecting training images...')
        real_names, real_tensor = knn.get_images(path_to_training_images, transform)
        with open(name_pth, 'wb') as fp:
            pickle.dump(real_names, fp)
    print('Collecting generating images...')
    generated_names, generated_tensor = knn.get_images(path_to_generated_images, transform)

    # extract features
    if not feature_flag:
        print('Extracting features from training images...')
        real_features = knn.feature_extractor(real_tensor, model, device)
        torch.save(real_features, feature_pth)
    print('Extracting features from generated images...')
    generated_features = knn.feature_extractor(generated_tensor, model, device)

    if sample == 'all':
        sample_size = len(generated_names)
    else:
        sample_size = int(sample)

    if mode == 'kNN':
        print('Finding kNNs...')
        knn.kNN(output_path,
            real_names, generated_names, 
            real_features, generated_features, 
            path_to_training_images, path_to_generated_images, 
            k=k_kNN, 
            sample=sample_size, 
            size=size,
            name_appendix=name_appendix)
    elif mode == 'pairs':
        print('Finding closest pairs...')
        knn.nearest_neighbor(output_path,
                        real_names, generated_names, 
                        real_features, generated_features, 
                        path_to_training_images, path_to_generated_images, 
                        sample=sample_size, 
                        size=size,
                        name_appendix=name_appendix)
    print('Finish!')


'''

if __name__ == '__main__':

    #device = "mps" if torch.backends.mps.is_available() else "cpu"
    device = "cuda" if torch.cuda.is_available() else "cpu"
    print('device:', device)
    print('Parsing arguments...')
    parser = argparse.ArgumentParser()
    parser.add_argument('-exp', '--exptpath', required=True, 
                        help='path to experiment', type=str)
    parser.add_argument('-rp', '--realpath', required=True, 
                        help='path to real images', type=str)
    parser.add_argument('-gp', '--genpath', required=True,
                        help='path to generated images', type=str)
    parser.add_argument('-d', '--data', nargs='?', const='lhq', default='lhq',
                        help='choose between "lhq" and "face" dataset', type=str)
    parser.add_argument('--size', nargs='?', const=128, default=128,
                        help='resolution of image the model was trained on, default 128 (int)', type=int)
    parser.add_argument('-a', '--arch', nargs='?', const='clip', default='clip', 
                        help='choose between "clip" and "cnn", default "clip"', type=str)
    parser.add_argument('-m', '--mode', nargs='?', const='kNN', default='kNN', 
                        help='choose between "kNN" and "pairs" for closest_pairs, default "kNN"', type=str)
    parser.add_argument('-k', '--k', nargs='?', const=3, default=3,
                        help='k for kNN, default 3', type=int)
    parser.add_argument('-s', '--sample', nargs='?', const=10, default=10,
                        help='how many generated samples to compare, default 10, int or "all"')
    parser.add_argument('-n', '--name', nargs='?', const='', default='', 
                        help='name appendix for the plot file', type=str)
    parser.add_argument('--fid', nargs='?', const='no', default='no',
                        help='compute FID and inception score, choose between "yes" and "no"', type=str)
    args = vars(parser.parse_args())

    path_to_experiment_folder = args['exptpath']
    path_to_real_images = args['realpath']
    path_to_generated_images = args['genpath']
    dataset = args['data']
    arch = args['arch']
    mode = args['mode']
    k_kNN = args['k']
    sample = args['sample']
    name_appendix = args['name']
    fid_bool = args['fid']
    size = args['size']

    ddpm_evaluator(experiment_path=path_to_experiment_folder, 
             realpath=path_to_real_images, 
             genpath=path_to_generated_images, 
             data=dataset, 
             arch=arch, 
             size=size,
             mode=mode, 
             k=k_kNN, 
             sample=sample, 
             name_appendix=name_appendix, 
             fid=fid_bool)

'''             