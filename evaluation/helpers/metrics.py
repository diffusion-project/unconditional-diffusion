import os
import torch
from tqdm import tqdm
from PIL import Image
from torchvision import transforms
from torch.utils.data import DataLoader
from itertools import cycle
from torchmetrics.image.fid import FrechetInceptionDistance
from torchmetrics.image.inception import InceptionScore
from torchmetrics.image.kid import KernelInceptionDistance
from torchmetrics.image import StructuralSimilarityIndexMeasure, PeakSignalNoiseRatio
from cleanfid import fid
from evaluation.helpers.score_infinity import calculate_FID_infinity_path, calculate_IS_infinity_path

def image_to_tensor(path, sample='all', device='cpu'):

    transform_resize = transforms.Compose([transforms.ToTensor(), transforms.Resize(128), transforms.Lambda(lambda x: (x * 255).type(torch.uint8))])
    transform = transforms.Compose([transforms.ToTensor(), transforms.Lambda(lambda x: (x * 255).type(torch.uint8)) ])
    filelist = os.listdir(path)
    if sample == 'all':
        sample_size = -1
    else:
        sample_size = sample
    image_list = []
    for file in filelist:
        if file.endswith('.png'):
            filepath = os.path.join(path, file)
            im = Image.open(filepath)
            if im.size[0] != 128:
                im = transform_resize(im)
            else: 
                im = transform(im)
            image_list.append(im)
            if len(image_list) == sample_size:
                break
    print(f'current sample size: {len(image_list)}')
    # convert list of tensors to tensor
    image_tensor = torch.stack(image_list).to(device)
    return image_tensor


def compute_scores(real, generated, device):

    real_dataloader = DataLoader(real, batch_size=128, shuffle=True)
    generated_dataloader = DataLoader(generated, batch_size=128, shuffle=True)

    fid = FrechetInceptionDistance().to(device)
    kid = KernelInceptionDistance().to(device)
    inception = InceptionScore().to(device)

    for r, g in zip(real_dataloader, cycle(generated_dataloader)):
        r = r.to(device)
        g = g.to(device)
        fid.update(r, real=True)
        fid.update(g, real=False)
        kid.update(r, real=True)
        kid.update(g, real=False)
        inception.update(g)
    
    fid_score = fid.compute()
    kid_score = kid.compute()
    is_score = inception.compute()
    return fid_score, kid_score, is_score


def clean_fid(path_to_real_images, path_to_generated_images):

    clean_fid_score = fid.compute_fid(path_to_real_images, path_to_generated_images, mode="clean", num_workers=0)
    clip_clean_fid_score = fid.compute_fid(path_to_real_images, path_to_generated_images, mode="clean", model_name="clip_vit_b_32")

    return clean_fid_score, clip_clean_fid_score


def fid_inf_is_inf(path_to_real_images, path_to_generated_images, batchsize=128):

    fid_infinity = calculate_FID_infinity_path(path_to_real_images, path_to_generated_images, batch_size=batchsize)
    is_infinity = calculate_IS_infinity_path(path_to_generated_images, batch_size=batchsize)

    return fid_infinity, is_infinity


def compute_ssim_psnr_scores(real, generated, device):
    real_dataloader = DataLoader(real, batch_size=128, shuffle=False)
    generated_dataloader = DataLoader(generated, batch_size=128, shuffle=False)

    ssim = StructuralSimilarityIndexMeasure(data_range=1.0).to(device)
    psnr = PeakSignalNoiseRatio().to(device)
    for r, g in zip(real_dataloader, cycle(generated_dataloader)):
        r = r.to(device)
        g = g.to(device)
        ssim.update(preds=g, target=r)
        psnr.update(preds=g, target=r)
    ssim_score = ssim.compute()
    psnr_score = psnr.compute()
    return psnr_score, ssim_score