# Evaluation Pipeline

We conduct two types of evaluation - qualitative and quantitative.

### Quantitative evaluations -

Quantitative metrics can be further categorised into two groups - content variant and content invariant metrics.

Content variant metrics are useful when the model can generate different samples from a noise vector. \
These evaluations are carried out to compare different backbone architectures of our unconditional diffusion model. \ 
A set of 10,000 generated samples from each model variant is compared with the test set of the real dataset. \
These evaluations include - 
1. FID score 
2. Inception score 
3. Clean FID score (with CLIP) 
4. FID infinity and IS infinity scores 

Content invariant metrics are useful when the model output can be compared w.r.t a ground truth. \
For example, our model can output the reconstructed version of an input training image (following the entire forward \
and reverse trajectories). \
These evaluation include -
1. SSIM (Structural Similarity Index Metric)
2. PSNR 


### Qualitative evaluations -

The aim of this set of evaluations is to qualitatively inspect whether our model has overfit to the training images. 
For this, the entire set of 10,000 generated samples from the best performing model from quanititative evaluation is 
compared with the training set of the real dataset. 
Additionally, the quality check is also done on a hand-selected subset of best generations. 

The comparison is implemented as MSE values between features of the generated and training samples. The features are 
extracted by using a pretrained model (ResNet50-Places365/VGGFace or CLIP). Based on the MSE scores we compute - 
1. kNN - plot the k nearest neighbors of the generated samples 
2. Closest pairs - plot the top pairs with smallest MSE value 


### Argumnets - 

Execution starts with evaluate_full.py file. Input arguments are - 

* <pre>-rp, --realpath : Path to real images (string) </pre>
* <pre>-gp, --genpath  : Path to generated images (string) </pre>
* <pre>-d, --data      : Choose between 'lhq' (for LHQ landscape dataset) and 'faces' (for CelebAHQ faces dataset). 
                         Default = 'lhq' (string)</pre>
* <pre>--size          : Resolution of images the model was trained on, default 128 (int) </pre>                  
* <pre>-a, --arch      : Choose between 'cnn' and 'clip'. Chosen pretrained model is used to extract features from the images.
                         If 'cnn' is selected, for LHQ dataset the model is a ResNet50 pretrained on Places365 dataset and for 
                         CelebAHQ dataset the model is a pretrained VGGFace. Not relevant in computing FID, IS scores. Default = 'clip' (string) </pre>
* <pre>-m, --mode      : Choose between 'kNN' and 'pairs' (for closest pairs), default = 'kNN' (string) </pre>
* <pre>-k, --k         : k value for kNN, default = 3 (int) </pre>
* <pre>-s, --sample    : Choose between an int and 'all'. If mode is 'kNN', plot kNN for this many samples (first s samples 
                         in the directory of generated images). If mode is 'pairs', plot the top s closest pairs from entire 
                         directory of generated images. Default 10 (int or 'all') </pre>
* <pre>-n, --name      : Name appendix (string) </pre>
* <pre>--fid           : Choose between 'yes' and 'no'. Compute FID, Inception score and upgraded FID scores. Default 'no' (string)   </pre>


Path to real images leads to a directory with two sub-directories - train and test.

<pre>
data 
|_ lhq 
|    |_ train 
|    |_ test 
|_ celebahq256_imgs 
|    |_ train 
|    |_ test 
</pre>
CLIP and CNN (ResNet50 or VGGFace) features of training images are saved after the first execution. This alleviates the need \
to recompute features of real images for different sets of generated samples.


### Links
1. ResNet50 pretrained on Places365 - https://github.com/CSAILVision/places365
2. Pretrained VGGFace - https://www.robots.ox.ac.uk/~vgg/software/vgg_face/
3. Clean FID - https://github.com/GaParmar/clean-fid/tree/main
4. FID infinity, IS infinity - https://github.com/mchong6/FID_IS_infinity/tree/master