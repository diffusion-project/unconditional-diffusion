import os
from pathlib import Path
import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from PIL import Image
import matplotlib.pyplot as plt
from collections import OrderedDict


class kNN():

    def __init__(self):
        pass

    def get_images(self, path, transform, size=128, *args, **kwargs):
        '''
        returns 
        names: list of filenames
        image_tensor: tensor with all images
        '''
        # path to real image files
        image_files = os.listdir(path)
        # list to store filenames
        names = []
        # list to store images (transformed to tensors)
        images_list = []
        
        for file in image_files:
            if file.endswith('.png'):
                filepath = os.path.join(path, file)
                names.append(file)
                im = Image.open(filepath)
                if im.size[0] != size:
                    im = im.resize((size,size))              # DDPM was trained on 128x128 images
                im = transform(im)  
                images_list.append(im)
        
        # tensor with all real image tensors
        image_tensor = torch.stack(images_list)

        return names, image_tensor

    def feature_extractor(self, images, model, device='cpu', bs=128, *args, **kwargs):
        '''
        returns
        real_features: VGGFace features for real images
        generated_features: VGGFace features for generated images
        '''
        # extract features for real and generated images
        dataloader = DataLoader(images, batch_size=bs, shuffle=False)
        features_list = []
        if model._get_name() == 'CLIP':
            with torch.no_grad():
                for item in dataloader:
                    features = model.encode_image(item.to(device))
                    features_list.append(features)
        else:
            with torch.no_grad():
                for item in dataloader:
                    features = model(item.to(device))
                    features_list.append(features)

        features = torch.cat(features_list, dim=0)
        return features


    def kNN(self, output_path, real_names, generated_names, 
            real_features, generated_features, 
            path_to_real_images, path_to_generated_images, 
            k=3, 
            sample=10, size=128,
            name_appendix='',
            *args, **kwargs):
        '''
        creates a plot with (generated image: k nearest real images) pairs
        '''
        if sample > 50:
            print('Cannot plot for more than 50 samples! sample <= 50')
        fig, ax = plt.subplots(sample, k+1, figsize=((k+1)*3,sample*2))

        for i in range(len(generated_features)):
            # l2 norm of one generated feature and all real features
            dist = torch.linalg.vector_norm(real_features - generated_features[i], ord=2, dim=1)
            
            # draw the generated image
            im = Image.open(os.path.join(path_to_generated_images, generated_names[i]))
            ax[i, 0].imshow(im)
            ax[i, 0].set_xticks([])
            ax[i, 0].set_yticks([])
            ax[i, 0].set_title(f'Generated: {generated_names[i].split("_")[2][:-4]}', fontsize=8)
            
            # kNN of the generated image
            knn = dist.topk(k, largest=False)
            j=1

            # draw the k real images
            for idx in knn.indices:
                im = Image.open(os.path.join(path_to_real_images, real_names[idx.item()]))
                if im.size[0] != size:
                    im = im.resize((size,size))
                ax[i, j].imshow(im)
                ax[i, j].set_xticks([])
                ax[i, j].set_yticks([])
                ax[i, j].set_title(f'{real_names[idx.item()][:-4]}, {knn.values[j-1].item():.2f}', fontsize=8)
                j += 1
            if i == sample-1:
                break
        
        # savefig
        if not output_path.is_dir():
            os.mkdir(output_path)
        plot_name = f'{k}NN_{sample}_samples'
        if name_appendix != '':
            plot_name = plot_name + '_' + name_appendix + '.png'
        fig.savefig(os.path.join(output_path, plot_name))

    def nearest_neighbor(self, output_path, real_names, generated_names, 
                    real_features, generated_features, 
                    path_to_real_images, path_to_generated_images, 
                    sample=10, size=128,
                    name_appendix='',
                    *args, **kwargs):
        
        print('Computing nearest neighbors...')
        if sample > 50:
            print('Cannot plot for more than 50 samples! sample <= 50')
        fig, ax = plt.subplots(sample, 2, figsize=(2*3,sample*2))
        nn_dict = OrderedDict()
        
        for i in range(len(generated_features)):
            # l2 norm of one generated feature and all real features
            #dist = torch.linalg.vector_norm(real_features - generated_features[i], ord=2, dim=1)   # no mps support
            dist = torch.norm(real_features - generated_features[i], dim=1, p=2)                    # soon to be deprecated
            
            # nearest neighbor of the generated image
            knn = dist.topk(1, largest=False)
            # insert to the dict: generated_image: (distance, index of the nearest neighbor)
            nn_dict[generated_names[i]] = (knn.values.item(), knn.indices.item())
        print('Finding closest pairs...')
        # sort to get the generated-real pairs that were the closest
        nn_dict_sorted = OrderedDict(sorted(nn_dict.items(), key=lambda item: item[1][0]))
        # names of the generated images that look closest to the real images
        gen_names = list(nn_dict_sorted.keys())
        print('Drawing the plot...')
        for i in range(sample):
            # draw the generated image
            im = Image.open(os.path.join(path_to_generated_images, gen_names[i]))
            ax[i, 0].imshow(im)
            ax[i, 0].set_xticks([])
            ax[i, 0].set_yticks([])
            ax[i, 0].set_title(f'Generated: {generated_names[i].split("_")[2][:-4]}', fontsize=8)
            
            # draw the real image
            knn_score, real_img_idx = nn_dict_sorted[gen_names[i]]
            im = Image.open(os.path.join(path_to_real_images, real_names[real_img_idx]))
            if im.size[0] != size:
                im = im.resize((size,size))
            ax[i, 1].imshow(im)
            ax[i, 1].set_xticks([])
            ax[i, 1].set_yticks([])
            ax[i, 1].set_title(f'{real_names[real_img_idx][:-4]}, {knn_score:.2f}', fontsize=8)
                
        #savefig
        if not output_path.is_dir():
            os.mkdir(output_path)
        plot_name = f'closest_pairs_top_{sample}'
        if name_appendix != '':
            plot_name = plot_name + '_' + name_appendix + '.png'
        fig.savefig(os.path.join(output_path, plot_name))
        