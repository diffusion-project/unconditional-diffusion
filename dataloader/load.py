import torch
from torch.utils.data import Dataset
from torchvision import transforms
import os 
from PIL import Image
import pandas as pd
import numpy as np

class UnconditionalDataset_LHQ(Dataset):
    def __init__(self,fpath,img_size,train,frac =0.8,skip_first_n = 0,ext = ".png",transform=True ):
        """
        Args:
            fpath (string):   Path to the folder where images are stored
            img_size (int):   Size of output image img_size=height=width
            ext (string):     Type of images used(eg .png)
            transform (Bool): Image augmentation for diffusion model
            skip_first_n:     Skips the first n values. Usefull for datasets that are sorted by increasing Likeliehood 
            train (Bool):     Choose dataset to be either train set or test set. frac(float) required 
            frac (float):     Value within (0,1] (seeded)random shuffles dataset, then divides into train and test set. 
        """

        ### Create DataFrame
        file_list = []
        for root, dirs, files in os.walk(fpath, topdown=False):
            for name in sorted(files):
                file_list.append(os.path.join(root, name))

        df = pd.DataFrame({"Filepath":file_list},)
        self.df = df[df["Filepath"].str.endswith(ext)] 
            
        if skip_first_n:
            self.df = self.df[skip_first_n:]
        
        if train: 
            df_train = self.df.sample(frac=frac,random_state=2)
            self.df = df_train
        else:
            df_train = self.df.sample(frac=frac,random_state=2)
            df_test = df.drop(df_train.index)
            self.df = df_test
            
        if transform: 
            intermediate_size = 150
            theta = np.pi/4 -np.arccos(intermediate_size/(np.sqrt(2)*img_size)) #Check dataloading.ipynb in analysis-depot for more details
            
            transform_rotate =  transforms.Compose([transforms.ToTensor(),
                                                    transforms.Resize(intermediate_size,antialias=True),
                                                    transforms.RandomRotation(theta/np.pi*180,interpolation=transforms.InterpolationMode.BILINEAR),
                                                    transforms.CenterCrop(img_size),
                                                    transforms.RandomHorizontalFlip(p=0.5), 
                                                    transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])
            
            transform_randomcrop  =  transforms.Compose([transforms.ToTensor(),
                                                         transforms.Resize(intermediate_size),
                                                         transforms.RandomCrop(img_size),
                                                         transforms.RandomHorizontalFlip(p=0.5),
                                                         transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])

            self.transform =  transforms.RandomChoice([transform_rotate,transform_randomcrop])
        else : 
            self.transform =  transforms.Compose([transforms.ToTensor(),
                                                  transforms.Resize(img_size)])
            
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self,idx):
        path =  self.df.iloc[idx].Filepaths
        img = Image.open(path)
        return self.transform(img),0
        
    def tensor2PIL(self,img):
        back2pil = transforms.Compose([transforms.Normalize(mean=(-1,-1,-1),std=(2,2,2)),transforms.ToPILImage()])
        return back2pil(img)
    
    
# Dataset used when training on CelebAHQ.    
class UnconditionalDataset_CelebAHQ(Dataset):
    def __init__(self,fpath,img_size,train,frac =0.8,skip_first_n = 0,ext = ".png",transform=True):
        """
        Args:
            fpath (string):   Path to the folder where images are stored
            img_size (int):   Size of output image img_size=height=width
            ext (string):     Type of images used(eg .png)
            transform (Bool): Image augmentation for diffusion model
            skip_first_n:     Skips the first n values. Usefull for datasets that are sorted by increasing Likeliehood 
            train (Bool):     Choose dataset to be either train set or test set. frac(float) required 
            frac (float):     Value within (0,1] (seeded)random shuffles dataset, then divides into train and test set. 
        """
        # they provide a fixed train and validation split
        if train:
            fpath = os.path.join(fpath, 'train') 
        else:
            fpath = os.path.join(fpath, 'valid')

        file_list =[]
        for root, dirs, files in os.walk(fpath, topdown=False):
            for name in sorted(files):
                file_list.append(os.path.join(root, name))
        df = pd.DataFrame({"Filepath":file_list},)
        self.df = df[df["Filepath"].str.endswith(ext)]
            
        if transform: 
            intermediate_size = 137
            theta = np.pi/4 -np.arccos(intermediate_size/(np.sqrt(2)*img_size)) #Check dataloading.ipynb in analysis-depot for more details
             
            transform_rotate_flip =  transforms.Compose([transforms.ToTensor(),
                                                         transforms.Resize(intermediate_size,antialias=True),
                                                         transforms.RandomRotation((theta/np.pi*180),interpolation=transforms.InterpolationMode.BILINEAR),
                                                         transforms.CenterCrop(img_size),
                                                         transforms.RandomHorizontalFlip(p=0.5),
                                                         transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])

            transform_flip =  transforms.Compose([transforms.ToTensor(),
                                                  transforms.Resize(img_size, antialias=True),
                                                  transforms.RandomHorizontalFlip(p=0.5),
                                                  transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])

            self.transform =  transforms.RandomChoice([transform_rotate_flip,transform_flip])                                                   
        else :
            self.transform =  transforms.Compose([transforms.ToTensor(),
                                                  transforms.Resize(img_size)])
            
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self,idx):
        path =  self.df.iloc[idx].Filepath
        img = Image.open(path)
        return self.transform(img),0
        
    def tensor2PIL(self,img):
        back2pil = transforms.Compose([transforms.Normalize(mean=(-1,-1,-1),std=(2,2,2)),transforms.ToPILImage()])
        return back2pil(img)
        